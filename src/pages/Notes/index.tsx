import React from 'react'
import styled from 'styled-components';
import GoBackArrow from '../../styled/GoBackArrow';
import { PrimarySubtitle } from '../../styled/PrimarySubtitle';
import { PrimaryTitle } from '../../styled/PrimaryTitle';

function Notes() {
  return (
    <Container>
      <GoBackArrow />
      <PrimaryTitle text="Notes" />
      <PrimarySubtitle text='I hope you had fun checking this mess of a code :D'/>
    </Container>
  );
}

export default Notes

const Container = styled.main`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.background.dark};
  height: 100vh;
  position: relative;
  padding: 20px;
`;