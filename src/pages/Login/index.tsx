import React, { useContext } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { animated, useSpring } from "react-spring";
import styled from "styled-components";
import Input from "../../components/Input";
import { AuthContext } from "../../contexts/AuthContext";
import useInput from "../../hooks/useInput";
import { PATHS } from "../../navigation/paths";
import PrimaryButton from "../../styled/PrimaryButton";

const WORDING = {
  title: "Welcome",
  subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  inputs: {
    email: {
      label: "User",
    },
    password: {
      label: "Password",
    },
  },
  forgotYourPassword: "Forgot your password?",
  loginButton: "Login",
  registerButton: "Register",
};

const Login = () => {
  const email = useInput({ initialValue: "" });
  const password = useInput({ initialValue: "" });
  const navigate = useNavigate();
  const { logIn, user } = useContext(AuthContext);

  const fadeInAnimation = useSpring({
    to: { opacity: 1 },
    from: { opacity: 0 },
    config: {
      duration: 1000
    }
  });

  
  if (user) {
    return <Navigate to={PATHS.HOME} replace />;
  };


  const {
    title,
    subtitle,
    inputs,
    forgotYourPassword,
    loginButton,
    registerButton,
  } = WORDING;

  async function handleSubmit(e: React.SyntheticEvent) {
    e.preventDefault();
    try {
      await logIn(email.value, password.value);
      navigate(PATHS.HOME);
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <Container>
      <Title>{title}</Title>
      <Subtitle>{subtitle}</Subtitle>
      <Form onSubmit={handleSubmit} style={fadeInAnimation}>
        <Input
          type={"email"}
          label={inputs.email.label}
          value={email.value}
          onChange={email.handleChange}
        />
        <Input
          type={"password"}
          label={inputs.password.label}
          value={password.value}
          onChange={password.handleChange}
        />
        <ForgotYourPassword to={"#"}>{forgotYourPassword}</ForgotYourPassword>
        <LoginButton onClick={handleSubmit} type="submit" text={loginButton} />
      </Form>
      <ButtonDivider>or</ButtonDivider>
      <RegisterButton onClick={() => navigate(PATHS.SIGN_IN)} type="button" text={registerButton} />
    </Container>
  );
};

export default Login;

const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  background-color: ${(props) => props.theme.background.dark};
  height: 100vh;
  padding: 0 31px;
`;

const Title = styled.h1`
  font-size: 42px;
  font-weight: 700;
  color: ${(props) => props.theme.text};
  text-align: center;
  margin: 0;
`;

const Subtitle = styled.p`
  font-family: "Epilogue";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  color: ${(props) => props.theme.text};
  text-align: center;
  opacity: 0.8;
  margin: 7px 0 23px 0;
`;

const ForgotYourPassword = styled(Link)`
  text-decoration: none;
  color: ${(props) => props.theme.link};
  opacity: 0.8;
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  margin: 22px 0;
  align-self: center;
`;

const LoginButton = styled(PrimaryButton)`
  background: linear-gradient(99deg, #236bfe 6.69%, #0d4ed3 80.95%);
  box-shadow: 0px 4px 30px rgba(34, 105, 251, 0.8);
`;

const RegisterButton = styled(PrimaryButton)`
  background: linear-gradient(99deg, #dc891d 6.69%, #de8510 80.95%);
  box-shadow: 0px 4px 30px rgba(251, 179, 34, 0.8);
`;

const Form = animated(styled.form`
  display: flex;
  flex-direction: column;
`);


const ButtonDivider = styled.p`
  padding: 10px;
  color: ${(props) => props.theme.text};
  opacity: 0.8;
  font-weight: bold;
  align-self: center;
`;