export const PATHS = {
  HOME: "/home",
  ONBOADRING: "green-run/",
  LOG_IN: "/login",
  SIGN_IN: "/signin",
  HISTORY: "/history",
  NOTES: "/notes",
  NOT_FOUND: "*",
};