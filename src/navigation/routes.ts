import { lazy, ReactNode } from "react";
import { PATHS } from "./paths";

// const PUBLIC_ROUTES = [
//   {
//     path: PATHS.LOG_IN,
//     element: lazy(() => import("../pages/LogIn")) as unknown,
//     exact: true,
//   },
//   {
//     path: PATHS.SIGN_IN,
//     element: lazy(() => import("../pages/SignIn")) as unknown,
//     exact: true,
//   },
//   {
//     path: PATHS.ONBOADRING,
//     element: lazy(() => import("../pages/Onboarding")) as unknown,
//     exact: true,
//   },
// ];

// const PRIVATE_ROUTES = [
//   {
//     path: PATHS.HOME,
//     element: lazy(() => import("../pages/Home")) as unknown,
//     exact: true,
//   },
//   {
//     path: PATHS.HISTORY,
//     element: lazy(() => import("../pages/History")) as unknown,
//     exact: true,
//   },
// ];

//   export {PUBLIC_ROUTES, PRIVATE_ROUTES}