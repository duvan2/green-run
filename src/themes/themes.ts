import { createGlobalStyle } from "styled-components";

  export const Colors = {
    white: "#FFFFFF",
  };

  export const dark = {
    name: "dark",
    text: "#FEFEFE",
    link: "#FEFEFE",
    background: {
      dark: "#181828",
      light: "#232232",
      input: "#2F2F43",
      item: "#212135",
    },
    input: {
      label: "#FEFEFE",
      value: "#FEFEFE",
    },
    bottomBanner: "#2C2B3E",
    navbar: {
      background: "#2C2B3E",
      selected: "#1F1F31",
      icon: {
        selected: "#FFFFFF",
        unselected: "#181828",
      },
    },
    icons: {
      like: "#FFFFFF",
      dislike: "#EA596F",
    },
    header: {
      backArrow: "#FFFFFF",
    },
    themeButtonBackground: "#222243",
    candidateNameText: "#FEFEFE",
  };

  export const light = {
    name: "light",
    text: "#161617",
    link: "#232232",
    background: {
      dark: "#E5E5E5",
      light: "#FFFFFF",
      input: "#FFFFFF",
      item: "#FFFFFF",
    },
    input: {
      label: "#3C3C3C",
      value: "#161617",
    },
    bottomBanner: "#FFFFFF",
    navbar: {
      background: "#FFFFFF",
      selected: "#FFFFFF",
      icon: {
        selected: "#2067F8",
        unselected: "#EDEDED",
      },
    },
    icons: {
      like: "#2067F8",
      dislike: "#EA596F",
    },
    header: {
      backArrow: "#232232",
    },
    themeButtonBackground: "#FFFFFF",
    candidateNameText: "#FEFEFE",
  };

  export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
  }
`;

  export const themesMap = { dark, light };