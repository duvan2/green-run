import { ReactNode, useContext } from "react";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { ThemeContext } from "../../contexts/ThemeStore";

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    transition: all 0.5s;
  }
`;

const Theme = ({ children }: { children: ReactNode }) => {
  const { theme } = useContext(ThemeContext);
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      {children}
    </ThemeProvider>
  );
};

export default Theme;