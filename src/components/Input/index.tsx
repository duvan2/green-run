import React, { ChangeEventHandler, HTMLInputTypeAttribute } from 'react'
import styled from 'styled-components';

interface InputProps {
  label: string;
  value: string;
  placeholder?: string;
  error?: string;
  required?: boolean;
  type?: HTMLInputTypeAttribute;
  onChange?: ChangeEventHandler<HTMLInputElement>;
}

export const Input = ({
  label,
  value = "",
  placeholder,
  error,
  required = true,
  type = "text",
  onChange,
}: InputProps) => {
  return (
    <InputContainer>
      <InputLabel>{label}</InputLabel>
      <CustomInput
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        required={required}
      ></CustomInput>
      {error !== "" && <p>{error}</p>}
    </InputContainer>
  );
};

const InputContainer = styled.div`
  background-color: ${(props) => props.theme.background.input};
  border: 1px solid rgba(255, 255, 255, 0.06);
  border-radius: 18px;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  margin: 5px 0;
  height: 50px;
  padding: 5px 16px;
`;

const InputLabel = styled.label`
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  color: ${(props) => props.theme.input.label};
  opacity: 0.6;
`;

const CustomInput = styled.input`
  font-size: 18px;
  color: ${(props) => props.theme.input.value};
  background-color: transparent;
  border-width: 0;
  width: 100%;
  border-bottom: 1px solid transparent;
  border-bottom: 1px solid rgba(254, 254, 254, 0.2);

  &:focus {
    outline: 0;
    border-width: 0;
    border-bottom: 1px solid rgba(254, 254, 254, 0.8);
  }

  transition: border-bottom 1s;
`;

export default Input;