import styled from "styled-components";
import Button from "../../components/Button";

const PrimaryButton = styled(Button)`
  background: linear-gradient(99deg, #236bfe 6.69%, #0d4ed3 80.95%);
  box-shadow: 0px 4px 30px rgba(34, 105, 251, 0.8);
  border-radius: 25px;
  gap: 10px;
  border-width: 0;
  padding: 22px 38px;
  align-self: center;
  color: white;
  font-size: 18px;
  font-weight: bold;
`;

export default PrimaryButton;
