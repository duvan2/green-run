import { css } from "styled-components";

export const SquareIconButton = css`
  width: 62px;
  height: 62px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 18px;
` 