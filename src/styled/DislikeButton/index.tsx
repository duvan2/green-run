import styled, { useTheme } from "styled-components";
import SvgIconWrapper from "../SvgIconWrapper";
import { ReactComponent as Cross } from "../../assets/svg/cross.svg";
import { Colors } from "../../themes/themes";

interface DislikeButtonProps {
  onPress: any;
}

const DislikeButton = ({ onPress }: DislikeButtonProps) => {
  const theme = useTheme();

  return (
    <ButtonContainer onClick={onPress}>
      <Icon height={14} fill={Colors.white} stroke={theme.icons.like}>
        <Cross />
      </Icon>
    </ButtonContainer>
  );
};

export default DislikeButton;

const ButtonContainer = styled.button`
    border: none;
    background: transparent;
    margin: 0 11px;
`;


const Icon = styled(SvgIconWrapper)`
  border-radius: 50%;
  width: 51px;
  height: 51px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #222243;
  box-shadow: 0px 10px 25px rgba(0, 0, 0, 0.08);
`;