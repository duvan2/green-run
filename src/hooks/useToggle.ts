import { useState } from "react";

interface UseToggleProps {
  initialState?: boolean;
}

const useToggle = ({ initialState = true }: UseToggleProps) => {
  const [isActive, toggleState] = useState<boolean>(initialState);

  function handleToggle() {
    toggleState((currentState) => !currentState);
  }

  return { isActive, handleToggle };
};

export default useToggle;
