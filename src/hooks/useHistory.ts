import { useEffect, useState, useContext } from "react";
import { AuthContext } from "../contexts/AuthContext";
import { getReactionHistory } from "../services/history";

const useHistory = () => {
    const [history, setHistory] = useState([]);
    const [loading, setLoading] = useState(false);

    const { user } = useContext(AuthContext);

    const getHistory = ({ uid }: { uid: string }) => {
      if (!user) return;
      try {
        getReactionHistory({ uid: uid });
      } catch (e) {
      } finally {
        setLoading(false);
      }
    };
  
    return { getHistory };
  };
  
  export default useHistory;
  