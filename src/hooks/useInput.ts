import { useState } from "react";

interface UseInputProps {
  initialValue: string;
  isRequired?: boolean;
}

const useInput = ({
  initialValue = "",
  isRequired = true,
}: UseInputProps): {
  value: string;
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  error: string;
  required: boolean;
  updateError: (error: string) => void;
} => {
  const [value, setValue] = useState<string>(initialValue);
  const [error, setError] = useState<string>("");
  const [required] = useState<boolean>(isRequired);

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    setValue(e.target.value);
  }

  function updateError(error: string) {
    setError(error);
  }

  return { value, handleChange, error, required, updateError };
};

export default useInput;
