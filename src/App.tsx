import "./App.css";
import { Route, Routes } from "react-router-dom";
import { ThemeStore } from "./contexts/ThemeStore";
import { AuthProvider } from "./contexts/AuthContext";
import Theme from "./components/Theme";
import { SportsProvider } from "./contexts/SportsContext";
import { PATHS } from "./navigation/paths";
import Home from "./pages/Home";
// import LogIn from "./pages/LogIn";
import Onboarding from "./pages/Onboarding";
import History from "./pages/History";
import Login from "./pages/Login";
import Notes from "./pages/Notes";
import SignIn from "./pages/SignIn";


function App() {
  return (
    <ThemeStore>
      <Theme>
        <AuthProvider>
          <SportsProvider>
            <Routes>
              <Route path={PATHS.LOG_IN} element={<Login />} />
              <Route path={PATHS.SIGN_IN} element={<SignIn />} />
              {/* <Route path={PATHS.NOT_FOUND} element={<NotFound />} /> */}
              <Route path={PATHS.ONBOADRING} element={<Onboarding />} />
              <Route path={PATHS.NOTES} element={<Notes />} />
              <Route path={PATHS.HOME} element={<Home />} />
              <Route path={PATHS.HISTORY} element={<History />} />
            </Routes>
          </SportsProvider>
        </AuthProvider>
      </Theme>
    </ThemeStore>
  );
}

export default App;

// const NotFound = () => (
//   <div>
//     <h1>page not found</h1>
//   </div>
// );
